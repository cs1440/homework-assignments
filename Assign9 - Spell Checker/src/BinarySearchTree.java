public class BinarySearchTree<E extends Comparable<E>> {
    private TreeNode<E> root;
    private int numNodes;

    public void display(String message) {
        System.out.println(message);
        this.displayInOrder(root);
        System.out.println();
    }

    private void displayInOrder(TreeNode<E> node) {
        if (node == null) return;

        displayInOrder(node.left);
        System.out.printf("%s, ", node.value);
        displayInOrder(node.right);
    }

    public boolean search(E search) {
        TreeNode<E> node = root;

        boolean found = false;
        while (!found && node != null) {
            if (node.value.compareTo(search) == 0) {
                found = true;
            } else if (node.value.compareTo(search) > 0) {
                node = node.right;
            } else {
                node = node.left;
            }
        }

        return found;
    }

    public boolean insert(E value) {
        boolean alreadyThere = search(value);
        if (alreadyThere) {
            return !alreadyThere;
        }
            if (root == null) {
                root = new TreeNode<E>(value);
            } else {
                // Search/find the insert location
                TreeNode<E> parent = null;
                TreeNode<E> node = root;
                while (node != null) {
                    parent = node;
                    if (node.value.compareTo(value) > 0) {
                        node = node.right;
                    } else {
                        node = node.left;
                    }
                }
                // Add the node to the tree
                TreeNode<E> newNode = new TreeNode<E>(value);
                if (parent.value.compareTo(value) > 0) {
                    parent.right = newNode;
                } else {
                    parent.left = newNode;
                }
            }
            numNodes++;
            return !alreadyThere;
        }

    public boolean remove(E value) {
        boolean isThere = search(value);
        if (!isThere) {
            return !isThere;
        }
        // Step 1: find the node to remove
        TreeNode<E> parent = null;
        TreeNode<E> node = root;
        boolean done = false;
        while (!done) {
            if (value.compareTo(node.value) > 0) {
                parent = node;
                node = node.left;
            }
            else if (value.compareTo(node.value) < 0) {
                parent = node;
                node = node.right;
            }
            else {
                done = true;
            }
        }

        // Step 2a: case for no left child
        if (node.left == null) {
            if (parent == null) {
                root = node.right;
            }
            else {
                if (value.compareTo(parent.value) > 0) {
                    parent.left = node.right;
                }
                else {
                    parent.right = node.right;
                }
            }
        }
        else { // Step 2b: case for left child
            TreeNode<E> parentOfRight = node;
            TreeNode<E> rightMost = node.left;
            while (rightMost.right != null) {
                parentOfRight = rightMost;
                rightMost = rightMost.right;
            }
            // Copy the largest value into the node being removed
            node.value = rightMost.value;
            if (parentOfRight.right == rightMost) {
                parentOfRight.right = rightMost.left;
            }
            else {
                parentOfRight.left = rightMost.left;
            }
        }
        numNodes--;
        return isThere;
    }

    public int numberNodes() {
        return numNodes;
    }
    //Kickoff method
    public int numberLeafNodes() {
        return numberLeafNodes(root);
    }

    public int numberLeafNodes(TreeNode<E> node) {
        if (node == null) return 0;
        if (node.left == null && node.right == null) {
            return 1;
        }
        return numberLeafNodes(node.left) + numberLeafNodes(node.right);
    }

    public int height() {
            return height(root) - 1;
    }

    public int height(TreeNode<E> node) {
        if (node == null) {
            return 0;
        }
        else if (node.left == null && node.right == null) {
            return 1;
        }
        int leftHeight = height(node.left);
        int rightHeight = height(node.right);
        if (leftHeight > rightHeight) {
            return leftHeight + 1;
        }
        else {
            return rightHeight + 1;
        }
    }


    private class TreeNode<E> {
        public E value;
        public TreeNode<E> left;
        public TreeNode<E> right;

        public TreeNode(E value) {
            this.value = value;
        }
    }
}
