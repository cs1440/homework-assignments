public class PatternGlider extends Pattern {
    private boolean[][] pattern = {{false, false, false, false, false},
            {false, false, false, true, false},
            {false, true, false, true, false},
            {false, false, true, true, false},
            {false, false, false, false, false}};
    public int getSizeX() {
        return 5;
    }
    public int getSizeY() {
        return 5;
    }
    public boolean getCell(int x, int y) {
        if ((x >= 0 && x <= 4) || (y >= 0 && y <= 4)) {
            return pattern[y][x];
        }
        else{
            return false;
        }
    }

}