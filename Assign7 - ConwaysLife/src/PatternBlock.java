public class PatternBlock extends Pattern {
    private boolean[][] pattern = {{true, true}, {true, true}};
    public int getSizeX() {
        return 2;
    }
    public int getSizeY() {
        return 2;
    }
    public boolean getCell(int x, int y) {
        if ((x == 0 || x == 1) && (y == 0 || y== 1)) {
            return pattern[y][x];
        }
        else {
            return false;
        }
    }

}