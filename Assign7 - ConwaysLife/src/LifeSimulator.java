public class LifeSimulator {
    private int sizeX;
    private int sizeY;
    private boolean[][] screen;

    public LifeSimulator(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.screen = new boolean[sizeY][sizeX];
    }

    public int getSizeX() {
        return sizeX;
    }
    public int getSizeY() {
        return sizeY;
    }

    public boolean getCell(int x, int y) {
        if ((x >= 0 && x <= sizeX - 1) && (y >= 0 && y <= sizeY - 1)) {
            return screen[y][x];
        }
        else {
            return false;
        }
    }

    public void insertPattern(Pattern pattern, int startX, int startY) {
        for (int i = 0; i <= pattern.getSizeY() - 1; i++) {
            System.out.println();
            for (int j = 0; j <= pattern.getSizeX() - 1; j++) {
                this.screen[startY + i][startX + j] = pattern.getCell(j, i);
            }
        }
    }
    public void update() {
        boolean[][] original = screen;
        boolean[][] updated = new boolean[sizeY][sizeX];
        for (int i = 0; i <= sizeY - 1; i++) {
            for (int j = 0; j <= sizeX - 1; j++) {
                updated[i][j] = thisSpot(j, i);
            }
        }
        this.screen = updated;
    }
    private boolean thisSpot(int x, int y) {
        int neighbors = 0;
        for (int i = 0; i <= 2; i++) {
            for (int j = 0; j <= 2; j++) {
                if (((y + i - 1) >= 0 && (y + i - 1) <= (sizeY - 1)) && ((x + j - 1) >= 0 && (x + j - 1) <= (sizeX - 1))) {
                    if (screen[y + i - 1][x + j - 1] && !(i == 1 && j == 1)) {
                        neighbors += 1;
                    }
                }
            }
        }
        if (neighbors < 2) {
            return false;
        }
        else if ((neighbors == 2 || neighbors == 3) && getCell(x, y)) {
            return true;
        }
        else if (neighbors >= 4) {
            return false;
        }
        else if (neighbors == 3 && !getCell(x, y)) {
            return true;
        }
        else {
            return false;
        }
    }

}