# magic number
# 6 is a magic number
# the factors are 1,2,3 and 1+2+3=6

import random
random.seed(1)
num1 = random.randint(1,8)

random.seed(2)
num2 = random.randint(1,8)

random.seed(4)
num3 = random.randint(1,8)

print("num1, num2, and num3: ", num1, num2, num3)


for i in range(10):
    if i % 2 == 0:
        continue
    for j in range(10):
        print(i*j, end=" ")
    print()
#Break command will only break the loop that it is inside

def sumAll(num1, num2):
    sum = 0
    for i in range(num1,num2+1):
        sum += i
    return sum

# Def defines logic and saves it for later

def main():
    print("sumAll(1,10): ", sumAll(1, 10))
    print("sumAll(1,10): ", sumAll(20, 37))
    print("sumAll(1,10): ", sumAll(35, 49))
    print("sumAll(1,10): ", sumAll(27, 59))

main()