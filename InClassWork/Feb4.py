import random

# I want a result of 3.14e+06
print(format(3141595, "10.2e"))

print(format(3141592, "10.2e"))
# Prints it with the right being justified (arrow direction)
print(format(3141592, ">10.2e"))
# Prints it with the left side justified
print(format(3141592, "<10.2e"))
# Prints it in the middle
print(format(3141592, "^10.2e"))

print(format(59832, "10d"))

print(format(59832, "<10d"))

# The x makes it transfer to hexidecimal
print(format(256, "10x"))

print(format(512, "<10x"))

print(format(18, "10x"))

print(format(187, "10x"))

bored = True

if bored == True:
    print("ur mom gay")
else:
    print("Yah yeet")

print(int(True))
print(int(False))

print(random.randint(1,6))

