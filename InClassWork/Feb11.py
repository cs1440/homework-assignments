import random
coin = random.randint(0,1)
if coin == 0:
    print("I win")
else:
    print("You lose")

# Use multiple if statements if multiple things can be true at the same time. Use multiple elif statements if they are mutually exclusive

year = eval(input("What year is it? "))
if year % 12 == 0:
    print("Monkey")
elif year % 12 == 1:
   print("rooster")
elif year % 12 == 2:
    print("dog")
elif year % 12 == 3:
    print("pig")
elif year % 12 == 4:
    print('rat')
elif year % 12 == 5:
    print('ox')
elif year % 12 == 6:
    print("tiger")
elif year % 12 == 7:
    print("rabbit")
elif year % 12 == 8:
    print("dragon")
elif year % 12 == 9:
    print("snake")
elif year % 12 == 10:
    print("horse")
elif year % 12 == 11:
    print("sheep")