def addTheNums(num):
    total = 0
    for i in range(1, num+1):
        total += i
    print(total)

def addNumsV2(num):
    if(num == 1):
        return num
    else:
        return num + addNumsV2(num-1)

def factorial(num):
    if(num==1):
        return num
    return num * factorial(num-1)

def fibonacci(num):
    return fibonacci(num-1) + fibonacci(num-2)

def plaindrome(str, idk):
    if idk == len(str)-1:
        print(str[idk], end="")
        return
    print(str[idk], end="")
    plaindrome(str, idk+1)
    print(str[idk], end="")

def main():
    addTheNums(100)
    print(factorial(10))

    str = "rats live on "
    plaindrome(str, 0)

    print()
    str = "prof e ssor "
    plaindrome(str, 0)

main()