class House:
    def __init__(self, squareFt = 2000, roomNums = 10):
        self.squareFt = squareFt
        self.roomNums = roomNums

    def getSquareFt(self):
        return self.squareFt

    def getRoomNums(self):
        return self.roomNums

    def setSquareFt(self, squareFt):
        self.squareFt = squareFt

    def setRoomNums(self, roomNums):
        self.roomNums = roomNums

def main():
    house1 = House()
    print("h1rooms: ",house1.getRoomNums(), "\nh1SqFeet: ",house1.getSquareFt())

    house2 = House(5000,20)
    print("h1rooms:", house2.getRoomNums(), "\nh2sqfeet:", house2.getSquareFt())
main()

wellOnExam = False
life = "good" if wellOnExam else "not good"
print("my life is", life)