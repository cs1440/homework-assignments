def max(num1, num2):
    if(num1>num2):
        return num1
    else:
        return num2

def main():
    print("Hello")
    print(max(5, 6))
    var1 = max(24, 12)
    print(var1)

main()

# Reference in Python is the address of the object in memory
def isPrime(number):
    divisor = 2
    while divisor <= number / 2:
        if number % divisor == 0:
            return False
        divisor += 1
    return True

for i in range(1,25):
    if isPrime(i):
        print(i)