def sum(num1,num2):
    return num1 + num2

def multiply(num3,num4):
    multTotal = 0
    for i in range(num4):
        multTotal = sum(multTotal,num3)
    return multTotal

def exponent(num5, num6):
    exp = 1
    for i in range(num6):
        exp = multiply(exp,num5)
    return exp

def polynomial(num1, num2, num3, num4, num5, num6, num7):
    total = 0
    total += exponent(num1, num2)
    total += exponent(num3,num4)
    total += exponent(num5,num6)
    total += num7
    return total
def main():
    print("Hello")
    x = sum(1,2)
    y = multiply(3,6)
    z = exponent(2,25)
    print("x:",x)
    print("y:", y)
    print("z:",z)
    a = polynomial(6, 3, 6, 2, 6, 1, 6)
    print("a:" , a)
main()