# Classes are the blueprints that build objects

import math

class Circle:
    def __init__(self, radius = 1):
        self.radius = radius

    def getRadius(self):
        return self.radius

    def setRadius(self, radius):
        self.radius = radius

    def getPerimeter(self):
        return 2* math.pi * self.radius

    def getArea(self):
        return math.pi * self.radius ** 2

def main():
    circ1 = Circle(5)
    c1area = circ1.getArea()

    print("c1area:" , c1area)

    circ2 = Circle(40003)
    c2area = circ2.getArea()
    print("c2area:", c2area)

    print(Circle(7).getRadius())

main()

class House:
    def __init__(self, squareFt = 2000, roomNum = 8):
        self.squareFt = squareFt
        self.roomNum = roomNum

    def getSquareFt(self):
        return self.squareFt

    def getRoomNum(self):
        return self.roomNum

    def setSquareFt(self, squareFt):
        self.squareFt = squareFt

    def setRoomNum(self, roomNum):
        self.roomNum = roomNum

def main2():
    house1 = House(10000)
    h1sqft = house1.getSquareFt()
    print(h1sqft)

main2()