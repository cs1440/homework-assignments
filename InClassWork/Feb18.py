# Difference between if statements and while loops are that while loops will repeat as long as the statement is true
import random

num1 = random.randint(1,30)
num2 = random.randint(1,30)

result = eval(input("Multiply " + str(num1) + " times " + str(num2) + " in your head. What is the answer? "))
while result != num1 + num2:
    result = eval(input("Sorry that is incorrect, please enter another value "))

print("correct!", result, "is the answer!!")

for k in range(1,102,2):
    print("k: ", k)

for yoMama in range(100,-1,-4):
    print("yoMama: ",yoMama)

number = 1
for n in range(1,1001):
    number *= n
print("number: ", number)

for i in range(11):
    for j in range(11):
        for k in range(11):
            for l in range(11):
                for m in range(11):
                    print(i*j*k*l*m)

num = 1000
result = 1
while num >0:
    result *= num
    num -= 1
print(result)