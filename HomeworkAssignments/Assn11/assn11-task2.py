import blobber
import time

def main():
    name = input("Enter your Blobber's name: ")
    color = input("Enter your Blobber's color: ")
    radius = eval(input("Enter your Blobber's radius: "))
    height = eval(input("Enter your Blobber's height: "))
    yourChild = blobber.Blobber(color, name, radius, height)
    startTime = time.time()
    currentTime = time.time()
    elapsedTime = currentTime - startTime
    while yourChild.vitalsOk(elapsedTime):
        print("Main Menu")
        print("\t(1): Display Name")
        print("\t(2): Change Name")
        print("\t(3): Display Color")
        print("\t(4): Change Color")
        print("\t(5): Feed Blobber")
        print("\t(6): Blobber Speak")
        print("\t(7): Exit")

        selection = int(input("Make a selection: "))

        if selection == 1:
            print("Your Blobber's name is " + yourChild.getName() + ".")
        elif selection == 2:
            name = input("Enter your Blobber's new name: ")
            yourChild.setName(name)
        elif selection == 3:
            print("Your Blobber's color is " + yourChild.getColor() + ".")
        elif selection == 4:
            color = input("Enter your Blobber's new color: ")
            yourChild.setColor(color)
        elif selection == 5:
            currentTime = time.time()
            elapsedTime = currentTime - startTime
            food = eval(input("Enter the amount to feed your Blobber: "))
            yourChild.theNewVolume(elapsedTime, food)
        elif selection == 6:
            currentTime = time.time()
            elapsedTime = currentTime - startTime
            print(yourChild.blobberSpeak(elapsedTime))
        else:
            break

    if eval(yourChild.happinessLevel(elapsedTime)) < 90:
        print("Your Blobber wasn't fed enough and turned to dust!")
    elif eval(yourChild.happinessLevel(elapsedTime)) > 110:
        print("Oh no! That was too much and now your Blobber is dunzo.")
    print("Thanks for taking care of a Blobber")




main()