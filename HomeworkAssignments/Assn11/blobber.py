from math import pi

class Blobber:
    def __init__(self, color, name, radius = 50, height = 100):
        self.__radius = radius
        self.__height = height
        self.__color = color
        self.__name = name
        self.__volume = pi * (radius ** 2) * height
        self.__newVolume = pi * (radius ** 2) * height

    def setName(self, name):
        self.__name = name

    def getName(self):
        return self.__name

    def setColor(self, color):
        self.__color = color

    def getColor(self):
        return self.__color

    def feedBlobber(self, food):
        return self.__radius + food

    def blobberSpeak(self, elapsedTime):
        if self.vitalsOk(elapsedTime):
            return ("My name is " + self.__name + " " + self.__color + ". My current happiness level is " + self.happinessLevel(elapsedTime) + "%")
        else:
            return ""

    def originalVolume(self):
        return pi * (self.__radius ** 2) * self.__height

    def theNewVolume(self, elapsedTime, food):
        self.__newVolume = pi * ((self.feedBlobber(food) - self.timeShrink(elapsedTime)) ** 2) * self.__height

    def happinessLevel(self, elapsedTime):
        return format(100 * (self.__newVolume /self.__volume), ".2f")

    def vitalsOk(self, elapsedTime):
        if (eval(self.happinessLevel(elapsedTime)) >= 90) and (eval(self.happinessLevel(elapsedTime)) <= 110):
            return True
        else:
            return False

    def timeShrink(self, elapsedTime):
        return (self.__radius * elapsedTime * 0.002)