import turtle


class Face:
    def __init__(self):
        self.__smile = True
        self.__happy = True
        self.__darkEyes = True

    def draw_face(self):
        turtle.clear()
        self.__drawHead()
        self.__drawEyes()
        self.__drawMouth()

    def isSmile(self):
        return self.__smile

    def isHappy(self):
        return self.__happy

    def isDarkEyes(self):
        return self.__darkEyes

    def changeMouth(self):
        self.__smile = not self.__smile
        self.draw_face()

    def changeEmotion(self):
        self.__happy = not self.__happy
        self.draw_face()

    def changeEyes(self):
        self.__darkEyes = not self.__darkEyes
        self.draw_face()

    def __drawHead(self):
        self.reset()
        turtle.penup()
        turtle.goto(0,-50)
        turtle.pendown()
        if self.__happy:
            turtle.color("black")
            turtle.fillcolor("yellow")
            turtle.begin_fill()
            turtle.circle(100)
            turtle.end_fill()
        else:
            turtle.color("black")
            turtle.fillcolor("red")
            turtle.begin_fill()
            turtle.circle(100)
            turtle.end_fill()

    def __drawEyes(self):
        self.reset()
        turtle.penup()
        turtle.goto(-30,75)
        if self.__darkEyes:
            turtle.fillcolor("black")
            turtle.begin_fill()
            turtle.circle(15)
            turtle.penup()
            turtle.goto(30,75)
            turtle.pendown()
            turtle.circle(15)
            turtle.end_fill()
        else:
            turtle.fillcolor("blue")
            turtle.begin_fill()
            turtle.circle(15)
            turtle.penup()
            turtle.goto(30,75)
            turtle.pendown()
            turtle.circle(15)
            turtle.end_fill()

    def __drawMouth(self):
        self.reset()
        turtle.penup()
        turtle.goto(50,15)
        turtle.pensize(5)
        turtle.color("black")
        turtle.pendown()
        if self.__smile:
            turtle.left(45)
            turtle.circle(75,-90)
        else:
            turtle.right(45)
            turtle.circle(-75,-90)

    def reset(self):
        turtle.setheading(0)
        turtle.color("black")
        turtle.pensize(1)


def main():
    face = Face()
    face.draw_face()

    done = False

    while not done:
        print("Change My Face")
        mouth = "frown" if face.isSmile() else "smile"
        emotion = "angry" if face.isHappy() else "happy"
        eyes = "blue" if face.isDarkEyes() else "black"
        print("1) Make me", mouth)
        print("2) Make me", emotion)
        print("3) Make my eyes", eyes)
        print("0) Quit")

        menu = eval(input("Enter a selection: "))

        if menu == 1:
            face.changeMouth()
        elif menu == 2:
            face.changeEmotion()
        elif menu == 3:
            face.changeEyes()
        else:
            break

    print("Thanks for Playing")

    turtle.hideturtle()
    turtle.done()


main()
