from modules.orbian import Orbian
from random import randint


def main():
    print("WELCOME TO ORBIAN FAMILY")
    print()

    family = []
    input("Hit Enter to Create the First Four Orbians")

    for i in range(0, 4):
        name = input("\tEnter a name for Orbian " + str(i+1) + ": ")
        family.append(Orbian(name))


    done = False

    while not done:
        print()
        print("Menu")
        print("\t1) Meet Orbian Family")
        print("\t2) Compare Orbians")
        print("\t3) Orbian Info")
        print("\t4) Create Orbian Baby")
        print("\t5) Send Orbian to Pasture")
        print("\t6) Orbian Thanos")
        print("\t7) Quit")
        choice = int(input("Choose an option: "))
        print()

        if choice == 1:
            listFamily(family) # here as an example
        elif choice == 2:
            compareOrbians(family)
        elif choice == 3:
            orbianInfo(family)
        elif choice == 4:
            orbianBaby(family)
        elif choice == 5:
            byeByeOrbian(family)
        elif choice == 6:
            print(orbianThanosSnap(family)) # This function call should return something
        elif choice == 7:
            done = True

    print("Thanks for playing Orbian Family!!!")

def listFamily(orbFamily):
    for i in range(len(orbFamily)):
        print("I am Orbian " + orbFamily[i].getName())

def selectOrbian(orbFamily, selected=-1):
    for i in range(len(orbFamily)):
        print("\t" + str(i + 1) + ") " + orbFamily[i].getName(), end="")
        if i == selected:
            print(" (already selected)")
        else:
            print()

    return int(input("Select an Orbian: ")) - 1

def compareOrbians(orbFamily):
    orbian1 = selectOrbian(orbFamily)
    orbian2 = selectOrbian(orbFamily, orbian1)
    if orbFamily[orbian1].getVolume() == orbFamily[orbian2].getVolume():
        print("\nOrbian " + orbFamily[orbian1].getName() + " and Orbian " + orbFamily[orbian2].getName() + " are the same size")
    elif orbFamily[orbian1].getVolume() > orbFamily[orbian2].getVolume():
        print("\nOrbian " + orbFamily[orbian1].getName() + " is bigger than Orbian " + orbFamily[orbian2].getName())
    elif orbFamily[orbian1].getVolume() < orbFamily[orbian2].getVolume():
        print("\nOrbian " + orbFamily[orbian2].getName() + " is bigger than Orbian " + orbFamily[orbian1].getName())

def orbianInfo(orbFamily):
    orbian = selectOrbian(orbFamily)
    print("\nOrbian " + str(orbFamily[orbian].getName()) + " is " + str(orbFamily[orbian].getAge()) + " zungs old")
    print("\tand is " + str(format(orbFamily[orbian].getVolume(), ".2f")) + " zogs, and " + str(len(orbFamily[orbian])) + " zings")

def orbianBaby(orbFamily):
    choice1 = selectOrbian(orbFamily)
    orbian1 = orbFamily[choice1]
    orbian2 = orbFamily[selectOrbian(orbFamily, choice1)]
    babyName = orbian1 + orbian2
    orbFamily.append(Orbian(babyName))
    print("\nGreetings Orbian " + babyName + ", child of " + orbian1.getName().title() + " and " + orbian2.getName().title())

def byeByeOrbian(orbFamily):
    dedOrbian = selectOrbian(orbFamily)
    print("\nFarewell dear " + orbFamily[dedOrbian].getName())
    orbFamily.pop(dedOrbian)

def orbianThanosSnap(orbFamily):
    for i in range(len(orbFamily)//2):
        orbFamily.pop(randint(0, len(orbFamily)-1))
    return "Orbian Thanos snapped his fingers and half the Orbians have died"


main()