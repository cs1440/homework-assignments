from math import pi
import time
from random import randint

class Orbian:
    def __init__(self, familyList, famVolume = 0, famHeight = 0, famAge = 0):
        self.__familyList = familyList
        self.__famVolume = famVolume
        self.__famAge = famAge
        self.__birthTime = time.time()
        self.__headRadius = randint(2, 5)
        self.__bodyRadius = randint(3, 8)
        self.__bodyHeight = randint(5, 15)
        headHeight = 2 * self.__headRadius
        totalVolume = (4 / 3) * pi * self.__headRadius ** 3 + pi * self.__bodyRadius ** 2 * self.__bodyHeight
        self.__famVolume = totalVolume
        totalHeight = headHeight + self.__bodyHeight
        self.__famHeight = totalHeight
        self.__grownUp = False

    def getName(self):
        return self.__familyList

    def __eq__(self, other):
        return self.__famVolume == other.__famVolume

    def __gt__(self, other):
        return self.__famVolume > other.__famVolume

    def __lt__(self, other):
        return self.__famVolume < other.__famVolume

    def getVolume(self):
        self.__setAges()
        if self.__famAge >= 2:

            while not self.__grownUp:

                self.__headRadius *= 2
                self.__bodyRadius *= 2
                self.__bodyHeight *= 6
                self.__famHeight = self.__headRadius * 2 + self.__bodyHeight

                volume = (4 / 3) * pi * (self.__headRadius ** 3) + pi * (self.__bodyRadius ** 2) * self.__bodyHeight
                self.__famVolume = volume
                self.__grownUp = True

        return self.__famVolume

    def getAge(self):
        self.__setAges()
        return self.__famAge

    def __len__(self):
        return int(self.__getHeight())

    def __getHeight(self):
        return self.__famHeight

    def __setAges(self):
        current = time.time()
        self.__famAge = (current - self.__birthTime) // 5

    def __add__(self, other):
        babyName = ""
        for i in range(len(self.getName() + other.getName()) // 2):
            if randint(1,2) == 1:
                babyName += self.getName()[randint(0,len(self.getName())-1)]
            else:
                babyName += other.getName()[randint(0,len(other.getName())-1)]
        return babyName.title()







