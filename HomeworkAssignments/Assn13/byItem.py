from modules.orbian import Orbian
from random import randint
from random import shuffle


def main():
    print("WELCOME TO ORBIAN FAMILY")
    print()

    family = []
    input("Hit Enter to Create the First Four Orbians")

    for i in range(0, 4):
        name = input("\tEnter a name for Orbian " + str(i+1) + ": ")
        family.append(Orbian(name))

    done = False

    while not done:
        print()
        print("Menu")
        print("\t1) Meet Orbian Family")
        print("\t2) Compare Orbians")
        print("\t3) Orbian Info")
        print("\t4) Create Orbian Baby")
        print("\t5) Send Orbian to Pasture")
        print("\t6) Orbian Thanos")
        print("\t7) Quit")
        choice = int(input("Choose an option: "))
        print()

        if choice == 1:
            listFamily(family) # here as an example
        elif choice == 2:
            compareOrbians(family)
        elif choice == 3:
            orbianInfo(family)
        elif choice == 4:
            orbianBaby(family)
        elif choice == 5:
            orbianPasture(family)
        elif choice == 6:
            print(orbianThanos(family))
        elif choice == 7:
            done = True

    print("Thanks for playing Orbian Family!!!")

def listFamily(familyList):
    for i in familyList:
        print("My name is Orbian " + i.getName())

def selectOrbian(family, selected=-1):
    num = 0
    for i in family:
        print("\t" + str(num + 1) + ") " + i.getName(), end="")
        if num == selected:
            print(" (already selected)")
        else:
            print()
        num += 1
    return int(input("Select an Orbian: ")) - 1

def orbianThanos(family):
    num = 0
    for i in family:
        num += 1
    num2 = num // 2
    while num > num2:
        num -= 1
        died = randint(0, num)
        family.pop(died)
    return "Orbian Thanos has snapped his fingers and half the Orbians have vanished"

def compareOrbians(family):
    orbian1 = selectOrbian(family)
    orbian2 = selectOrbian(family, orbian1)
    num = 0
    for i in family:
        if orbian1 == num:
            compareOrb1 = i
            break
        else:
            num += 1
    num = 0
    for i in family:
        if orbian2 == num:
            compareOrb2 = i
            break
        else:
            num += 1

    if compareOrb1 == compareOrb2:
        print("\nOrbian " + compareOrb1.getName() + " and Orbian " + compareOrb2.getName() + " are the same size")
    elif compareOrb1 > compareOrb2:
        print("\nOrbian " + compareOrb1.getName() + " is taller than " + compareOrb2.getName())
    elif compareOrb1 < compareOrb2:
        print("\nOrbian " + compareOrb1.getName() + " is shorter than " + compareOrb2.getName())

def orbianInfo(family):
    orbian = selectOrbian(family)
    num = 0
    for i in family:
        if orbian == num:
            print("\nOrbian " + (i.getName()) + " is " + str(i.getAge()) + " zungs old")
            print("\tand is " + str(format(i.getVolume(), ".2f")) + " zogs, and " + str(len(i)) + " zings")
            break
        else:
            num += 1

def orbianBaby(family):
    orbian1 = selectOrbian(family)
    orbian2 = selectOrbian(family, orbian1)
    num = 0
    for i in family:
        if orbian1 == num:
            parent1 = i
            break
        else:
            num += 1
    num = 0
    for i in family:
        if orbian2 == num:
            parent2 = i
            break
        else:
            num += 1

    baby = parent1 + parent2
    family.append(Orbian(baby))
    print("\nWelcome Orbian " + baby + ", child of " + parent1.getName() + " and " + parent2.getName())

def orbianPasture(family):
    orbian = selectOrbian(family)
    name = family.pop(orbian).getName()
    print("\nFarewell dear " +  name)


main()