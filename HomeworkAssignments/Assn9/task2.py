from chessboard import drawChessBoard
def main():
    x, y = eval(input("Enter starting x and y coordinates: "))
    length = input("Enter a desired length for your chessboard: ")
    height = input("Enter a desired height for your chessboard: ")
    if (length == "") and (height == ""):
        drawChessBoard(x, y)
    elif length == "":
        drawChessBoard(x, y, height= eval(height))
    elif height == "":
        drawChessBoard(x, y, length= eval(length))
    else:
        drawChessBoard(x, y, length= eval(length), height= eval(height))

main()