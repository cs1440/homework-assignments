import turtle

def drawChessBoard(x, y, length=250,height=250):
    turtle.penup()
    turtle.goto(x,y)
    turtle.pendown()
    mainRectangle(length,height)
    for i in range(4):
        drawAllRectangles(length,height)
        turtle.left(90)
        turtle.forward(height/4)
        turtle.left(90)
        turtle.forward(length)
        turtle.right(180)
    turtle.penup()
    turtle.goto(x+length/8,y+height/8)
    for i in range(4):
        drawAllRectangles(length,height)
        turtle.left(90)
        turtle.forward(height/4)
        turtle.left(90)
        turtle.forward(length)
        turtle.right(180)
    turtle.done()

def drawAllRectangles(length,height):
    for i in range(4):
        turtle.pendown()
        turtle.begin_fill()
        drawRectangle(length,height)
        turtle.end_fill()
        turtle.penup()
        turtle.forward(length/4)

def drawRectangle(length,height):
    turtle.speed(10)
    turtle.color("black")
    turtle.forward(length/8)
    turtle.left(90)
    turtle.forward(height/8)
    turtle.left(90)
    turtle.forward(length/8)
    turtle.left(90)
    turtle.forward(height/8)
    turtle.left(90)

def mainRectangle(length,height):
    turtle.color("red")
    turtle.forward(length)
    turtle.left(90)
    turtle.forward(height)
    turtle.left(90)
    turtle.forward(length)
    turtle.left(90)
    turtle.forward(height)
    turtle.left(90)
