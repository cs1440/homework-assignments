class Wordinator:
    def __init__(self, word1, word2):
        self.__word1 = word1
        self.__word2 = word2

    def firstWord(self):
        if self.__word1 > self.__word2:
            return self.__word2
        else:
            return self.__word1

    def bigWord(self):
        return self.__word1 + self.__word2

    def wordsFactor(self, times):
        output = "Word factor one is " + (self.__word1 * times) + "\n" + "Word factor two is " + (self.__word2 * times)
        return output

    def mixWords(self):
        output = ""
        if len(self.__word1) > len(self.__word2):
            for i in range(len(self.__word2)):
                output += self.__word1[i]
                output += self.__word2[i]
            output += self.__word1[i+1:]
        else:
            for i in range(len(self.__word1)):
                output += self.__word1[i]
                output += self.__word2[i]
            output += self.__word2[i+1:]
        return output

    def midWords(self):
        if len(self.__word1[:int(len(self.__word1)/3)+1]) == len(self.__word1[int(len(self.__word1)*2/3):]) and len(self.__word2[:int(len(self.__word2)/3)+1]) == len(self.__word2[int(len(self.__word2)*2/3):]):
            output = "Midword one is "
            output += self.__word1[int(len(self.__word1)/3):int(len(self.__word1)*2/3)+1]
            output += "\nMidword two is "
            output += self.__word2[int(len(self.__word2)/3):int(len(self.__word2)*2/3)+1]

        elif len(self.__word1[:int(len(self.__word1)/3)+1]) != len(self.__word1[int(len(self.__word1)*2/3):]) and len(self.__word2[:int(len(self.__word2)/3)+1]) == len(self.__word2[int(len(self.__word2)*2/3):]):
            output = "Midword one is "
            output += self.__word1[int(len(self.__word1)/3)-1:int(len(self.__word1)*2/3)+1]
            output += "\nMidword two is "
            output += self.__word2[int(len(self.__word2)/3):int(len(self.__word2)*2/3)+1]

        elif len(self.__word1[:int(len(self.__word1)/3)+1]) == len(self.__word1[int(len(self.__word1)*2/3):]) and len(self.__word2[:int(len(self.__word2)/3)+1]) != len(self.__word2[int(len(self.__word2)*2/3):]):
            output = "Midword one is "
            output += self.__word1[int(len(self.__word1)/3):int(len(self.__word1)*2/3)+1]
            output += "\nMidword two is "
            output += self.__word2[int(len(self.__word2)/3)-1:int(len(self.__word2)*2/3)+1]

        elif len(self.__word1[:int(len(self.__word1)/3)+1]) != len(self.__word1[int(len(self.__word1)*2/3):]) and len(self.__word2[:int(len(self.__word2)/3)+1]) != len(self.__word2[int(len(self.__word2)*2/3):]):
            output = "Midword one is "
            output += self.__word1[int(len(self.__word1)/3)-1:int(len(self.__word1)*2/3)+1]
            output += "\nMidword two is "
            output += self.__word2[int(len(self.__word2)/3)-1:int(len(self.__word2)*2/3)+1]
        return output

    def switchCaps(self):
        alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        result = "The first word with switched caps is "
        for x in self.__word1:
            for position in range(52):
                if x == alphabet[position] and position <= 25:
                    result += alphabet[position+26]
                elif x == alphabet[position] and position > 25:
                    result += alphabet[position-26]
        result += "\nThe second word with switched caps is "
        for s in self.__word2:
            for position in range(52):
                if s == alphabet[position] and position <= 25:
                    result += alphabet[position+26]
                elif s == alphabet[position] and position > 25:
                    result += alphabet[position-26]
        return result


    def backwards(self):
        output = "The first word backwards is " + self.__word1[::-1]
        output += "\nThe second word backwards is " + self.__word2[::-1]
        return output

    def backwardsManual(self):
        output = "The first reversed string is "

        str = ""
        for i in self.__word1:
            str = i + str

        output += str
        output += "\nThe second reversed string is "

        str2 = ""
        for j in self.__word2:
            str2 = j + str2
        output += str2

        return output
