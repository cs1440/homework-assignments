class Chessboard:
    def __init__(self, tr, startX, startY, width=100, height=100):
        self.__tr = tr
        self.__startX = startX
        self.__startY = startY
        self.__width = width
        self.__height = height

    def __drawRectangle(self):
        self.__tr.begin_fill()
        for i in range(2):
            self.__tr.forward(self.__width/8)
            self.__tr.left(90)
            self.__tr.forward(self.__height/8)
            self.__tr.left(90)
        self.__tr.end_fill()

    def __drawRow(self):
        for i in range(4):
            self.__drawRectangle()
            self.__tr.penup()
            self.__tr.forward(self.__width/4)
            self.__tr.pendown()

    def __drawEvenRows(self):
        for i in range(4):
            self.__drawRow()
            self.__tr.left(90)
            self.__tr.penup()
            self.__tr.forward(self.__height/4)
            self.__tr.left(90)
            self.__tr.forward(self.__width)
            self.__tr.pendown()
            self.__tr.setheading(0)

    def __drawOddRows(self):
        self.__tr.penup()
        self.__tr.goto(self.__startX+(self.__width/8),self.__startY+(self.__height/8))
        self.__tr.pendown()
        self.__drawEvenRows()

    def __setup(self):
        self.__tr.penup()
        self.__tr.goto(self.__startX, self.__startY)
        self.__tr.pendown()
        self.__tr.speed(11)
        for i in range(2):
            self.__tr.forward(self.__width)
            self.__tr.left(90)
            self.__tr.forward(self.__height)
            self.__tr.left(90)

    def draw(self):
        self.__setup()
        self.__drawEvenRows()
        self.__drawOddRows()
