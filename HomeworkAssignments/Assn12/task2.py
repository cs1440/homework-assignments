from modules.wordinator import Wordinator

def main():
    print("Welcome to the Wordinator")
    word1 = input("Enter the first word: ")
    word2 = input("Enter the second word: ")

    wordinator = Wordinator(word1, word2)

    done = False

    while not done:
        print()
        print("Menu")
        print("\t1) First Word")
        print("\t2) Big Word")
        print("\t3) Words Factor")
        print("\t4) Mix Words")
        print("\t5) Middle Words")
        print("\t6) Switched Case Words")
        print("\t7) Back Words Sliced")
        print("\t8) Back Words Manual")
        print("\t9) Quit")
        mode = int(input("Enter Selection: "))
        print()

        if mode == 1:
            firstWord(wordinator)
        elif mode == 2:
            bigWord(wordinator)
        elif mode == 3:
            wordsFactor(wordinator, times = eval(input("Enter an integer factor value: ")))
        elif mode == 4:
            mixWords(wordinator)
        elif mode == 5:
            midWords(wordinator)
        elif mode == 6:
            switchCaps((wordinator))
        elif mode == 7:
            backwards(wordinator)
        elif mode == 8:
            backwardsManual(wordinator)
        elif mode == 9:
            done = True

    print("Thanks for Playing Wordinator")

def firstWord(wordinatorObject):
    print("The first word is " + wordinatorObject.firstWord())

def bigWord(wordinatorObject):
    print("The big word is " + wordinatorObject.bigWord())

def wordsFactor(wordinatorObject, times):
    print(wordinatorObject.wordsFactor(times))

def mixWords(wordinatorObject):
    print("The mixed word is " + wordinatorObject.mixWords())

def midWords(wordinatorObject):
    print(wordinatorObject.midWords())

def switchCaps(wordinatorObject):
    print(wordinatorObject.switchCaps())

def backwards(wordinatorObject):
    print(wordinatorObject.backwards())

def backwardsManual(wordinatorObject):
    print(wordinatorObject.backwardsManual())



main()