import pattern
from random import randint

def main():
    pattern.setup()
    while True:
        print("1) Rectangle\n2) Circle\n3) Super Pattern")
        drawMode = eval(input("Which game mode would you like to play? 1, 2, or 3? "))
        if drawMode == 1:
            x, y = eval(input("Enter starting x and y coordinates: "))
            offset = eval(input("Offset? "))
            width = eval(input("Width? "))
            height = eval(input("Height? "))
            count = eval(input("Count? "))
            rotation = eval(input("Rotation? "))
            pattern.drawRectanglePattern(x, y, width, height, offset, count, rotation)

        elif drawMode == 2:
            x, y = eval(input("Enter starting x and y coordinates: "))
            offset = eval(input("Offset? "))
            radius = eval(input("Radius? "))
            count = eval(input("Count? "))
            pattern.drawCirclePattern(x, y, radius, offset, count)

        else:
            pattern.drawSuperPattern(number = eval(input("Number? ")))

        print("Want to play again?\n1) Yes, and keep drawings\n2) Yes, and clear drawings\n3) No, I'm all done")
        userContinue = eval(input("Choose option 1, 2, or 3: "))
        if userContinue == 1:
            continue
        elif userContinue == 2:
            pattern.reset()
            continue
        else:
            print("Thanks for playing!")
            pattern.done()
            break
main()