import turtle
from random import randint
from math import pi

def reset():
    turtle.clearscreen()

def setup():
    turtle.speed(30)
    turtle.setup(1000, 800)

def drawRectanglePattern(x, y, width, height, offset, repetitions, rotation):
    setup()
    turtle.penup()
    turtle.goto(x, y)
    turtle.forward(offset)
    turtle.pendown()
    turtle.left(rotation)
    for i in range(repetitions):
        setRandomColor()
        drawRectangle(width,height)
        offsetPattern(offset, repetitions)


def drawRectangle(width, height):
    turtle.forward(width)
    turtle.left(90)
    turtle.forward(height)
    turtle.left(90)
    turtle.forward(width)
    turtle.left(90)
    turtle.forward(height)
    turtle.left(90)

def offsetPattern(offset, repetitions):
    turtle.left(360 / repetitions)
    turtle.penup()
    turtle.forward(2 * pi * offset / repetitions)
    turtle.pendown()

def drawCirclePattern(x, y, radius, offset, repetitions):
    setup()
    turtle.penup()
    turtle.goto(x, y)
    turtle.forward(offset-radius)
    turtle.pendown()
    turtle.setheading(0)
    for i in range(repetitions):
        setRandomColor()
        turtle.circle(radius)
        offsetPattern(offset, repetitions)

def drawSuperPattern(number):
    for i in range(number):
        rectangleOrCircle = randint(1, 2)
        if rectangleOrCircle == 1:
            drawRectanglePattern(x = randint(-400, 400), y = randint(-300, 300), width = randint(1,100), height = randint(1,100), offset = randint(-50,50), repetitions = randint(2,50), rotation = randint(0,360))
        else:
            drawCirclePattern(x = randint(-400,400), y = randint(-300,300), radius = randint(1,50), offset = randint(-100,100), repetitions = randint(2,75))
def setRandomColor():
    colorNumber = randint(1,4)
    if colorNumber == 1:
        turtle.color("red")
    elif colorNumber == 2:
        turtle.color("purple")
    elif colorNumber == 3:
        turtle.color("navy")
    else:
        turtle.color("green")

def done():
    turtle.done()


