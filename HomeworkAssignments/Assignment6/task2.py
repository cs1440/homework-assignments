# User input values
employeeName = input("Enter the employee's name: ")
hoursWorked = eval(input("Enter the number of hours " + employeeName + " worked: "))
hourlyPay = eval(input("Enter " + employeeName + "'s hourly pay: "))
fedTax = eval(input("Enter the federal tax withholding rate (ex. 0.11): "))
stateTax = eval(input("Enter the state tax withholding rates (ex. 0.05): "))

# Calculations
grossPay = round(hoursWorked * hourlyPay, 2)
fedDeductions = round(grossPay * fedTax, 2)
stateDeductions = round(grossPay * stateTax, 2)
totalDeductions = fedDeductions + stateDeductions
netPay = grossPay - totalDeductions
fedPercent = format(fedTax, ".1%")
statePercent = format(stateTax, ".1%")

# Formatting specifications
farRight = ">15.2f"
textAlign = ">30s"
dollarSign = "  $"

# This does all the formatting
payInformation = ("\n" + format(employeeName + "'s Pay Information", ">40s").upper() + "\n")
payCaption = (format("Pay", "^50s"))
hoursPrint = (format("Hours Worked:", textAlign) + "   " + format(str(hoursWorked), ">15s"))
hourlyPayPrint = (format("Hourly Pay:", textAlign) + dollarSign + format(hourlyPay, farRight))
grossPayPrint = (format("Gross Pay:", textAlign) + dollarSign + format(grossPay, farRight))
deductionCaption = (format("Deductions", "^50s"))
fedWithholdPrint = (format("Federal Withholding " + "(" + str(fedPercent) + "):", textAlign) + dollarSign + format(fedDeductions, farRight))
stateWithholdPrint = (format("State Wittholding " + "(" + str(statePercent) + "):", textAlign) + dollarSign + format(stateDeductions, farRight))
totDeductionPrint = (format("Total Deduction:", textAlign) + dollarSign + format(totalDeductions, farRight))
netPayPrint = ("\n" + format("Net Pay:", textAlign) + dollarSign + format(netPay, farRight))

# Final print statement combination
finalPrint = (payInformation + "\n" + payCaption + "\n" + hoursPrint + "\n" + hourlyPayPrint + "\n" + grossPayPrint + "\n" + deductionCaption + "\n" + fedWithholdPrint + "\n" + stateWithholdPrint + "\n" + totDeductionPrint + "\n" + netPayPrint)

print(finalPrint)