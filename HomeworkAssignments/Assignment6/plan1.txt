Requirements specification: This code should ask the user for inputs of the number of sides of a polygon and the length
of one of the sides, then return the area of the shape

System analysis: The system should take the number of sides (n) and the length of a side and plug it into the equation
to get Area=(n*s*s)/(4*tan(pi/n)). The math library will need to be imported to use the functions of powers, tangent, and pi

System design: Tell user that the program will help them calculate the area of a given regular polygon. Then it will ask
for the user to input the number of sides of the shape, then will ask for the length of one of the sides. Afterwards the
program will print the total area in units squared of the polygon, rounded to the 4th decimal point

Testing:
TEST ONE: Input: n = 8, s = 3. Output should be, according to Google, approximately area = 43.46 units squared
TEST TWO Input: n = 4, s = 8. Output should be 64, because the shape is a square, so multiply a side by a side to get 64 units squared
PASSED