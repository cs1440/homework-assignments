import math
from math import pi
from math import tan

n = eval(input("Please input the number of sides of your polygon: "))
s = eval(input("What is the length of one of the sides? "))
polyArea = (n * math.pow(s,2))/(4 * tan(pi/n))
print("The area of your polygon is:" , round(polyArea,5) , "units squared")