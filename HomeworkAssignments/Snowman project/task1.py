# Levi Nielson, A02253509. This program will print, in turtle, a snowman with various features. His name is Steve

import turtle

turtle.bgcolor("gray")

#This is the body
turtle.speed(11)
turtle.color("white")
turtle.begin_fill()
turtle.circle(75)
turtle.end_fill()
turtle.penup()
turtle.goto(0 , -200)
turtle.pendown()
turtle.begin_fill()
turtle.circle(100)
turtle.end_fill()
turtle.penup()
turtle.goto(0, 150)
turtle.pendown()
turtle.begin_fill()
turtle.circle(50)
turtle.end_fill()

#This is the eyes
turtle.penup()
turtle.goto(-15,200)
turtle.pendown()
turtle.color("black")
turtle.begin_fill()
turtle.circle(10)
turtle.end_fill()
turtle.penup()
turtle.goto(15,200)
turtle.pendown()
turtle.begin_fill()
turtle.circle(10)
turtle.end_fill()

turtle.penup()
turtle.goto(0,190)
turtle.pendown()
turtle.begin_fill()
turtle.color("orange")
turtle.right(15)
turtle.forward(50)
turtle.left(165)
turtle.forward(50)
turtle.setheading(0)
turtle.end_fill()

# This is the mouth
turtle.penup()
turtle.color("black")
turtle.goto(-20,175)
turtle.pendown()
turtle.goto(0,160)
turtle.goto(20,175)

# This is his hat
turtle.penup()
turtle.goto(-75 , 245)
turtle.pendown()
turtle.begin_fill()
turtle.forward(150)
turtle.left(90)
turtle.forward(50)
turtle.left(90)
turtle.forward(150)
turtle.left(90)
turtle.forward(50)
turtle.end_fill()

turtle.penup()
turtle.goto(-25,295)
turtle.pendown()


turtle.begin_fill()
turtle.left(180)
turtle.forward(50)
turtle.right(90)
turtle.forward(50)
turtle.right(90)
turtle.forward(50)
turtle.right(90)
turtle.forward(50)
turtle.end_fill()

# This is the arm
turtle.penup()
turtle.color("chocolate")
turtle.goto(-50,75)
turtle.pendown()
turtle.begin_fill()
turtle.left(45)
turtle.forward(100)
turtle.right(15)
turtle.forward(30)
turtle.left(165)
turtle.forward(30)
turtle.right(140)
turtle.forward(30)
turtle.left(165)
turtle.forward(30)
turtle.right(140)
turtle.forward(30)
turtle.left(160)
turtle.forward(35)
turtle.right(15)
turtle.forward(100)
turtle.end_fill()

turtle.penup()
turtle.goto(50, 110)
turtle.pendown()
turtle.right(30)
turtle.begin_fill()
turtle.forward(100)
turtle.right(15)
turtle.forward(30)
turtle.left(165)
turtle.forward(30)
turtle.right(140)
turtle.forward(30)
turtle.left(165)
turtle.forward(30)
turtle.right(140)
turtle.forward(30)
turtle.left(160)
turtle.forward(35)
turtle.right(15)
turtle.forward(100)
turtle.end_fill()

turtle.penup()
turtle.home


turtle.color("red")
turtle.goto(0,90)
turtle.pendown()
turtle.setheading(0)
turtle.begin_fill()
turtle.circle(10)
turtle.end_fill()

turtle.penup()
turtle.goto(0,60)
turtle.pendown()
turtle.begin_fill()
turtle.circle(10)
turtle.end_fill()

turtle.penup()
turtle.goto(0,30)
turtle.pendown()
turtle.begin_fill()
turtle.circle(10)
turtle.end_fill()
turtle.right(90)

turtle.done()




