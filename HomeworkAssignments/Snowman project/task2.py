# Levi Nielson, A02253509. This program will ask you for lengths of a 3D cuboid and will give you the dimensions in return

print("Hello there! I am a cuboid calculator, or in other words, I can help you calculate the volume and surface area "
      "of any cube-like object.")
print("Please input your values in ft")
length = eval(input("Length of object: "))
width = eval(input("Width of object: "))
height = eval(input("Height of object: "))
volume = length * width * height
surface_area = 2 * (length * width + length * height + width * height)
print("Your" , length , "x" , width , "x" , height , "object has a volume of" , volume , "cubic feet and a surface area of" , surface_area , "square feet")
