import turtle
print("Hello I'm here to help you create your own custom target")
center_Xvar , center_Yvar = eval(input("What are your desired x and y coordinates for the center of your target? (ex. 0,0 ; 10,-100) "))
bullseye_radius = eval(input("What is the desired radius for your bullseye? "))
second_layer = bullseye_radius + bullseye_radius
third_layer = second_layer + bullseye_radius
outer_layer = third_layer + bullseye_radius

turtle.penup()
turtle.goto(center_Xvar , center_Yvar - outer_layer)
turtle.speed(11)

# This is the outer layer
turtle.color("black")
turtle.pendown()
turtle.begin_fill()
turtle.circle(outer_layer)
turtle.end_fill()

# This is the third layer
turtle.penup()
turtle.goto(center_Xvar,center_Yvar - third_layer)
turtle.pendown()
turtle.color("blue")
turtle.begin_fill()
turtle.circle(third_layer)
turtle.end_fill()

# This is the second layer
turtle.color("red")
turtle.penup()
turtle.goto(center_Xvar,center_Yvar - second_layer)
turtle.pendown()
turtle.begin_fill()
turtle.circle(second_layer)
turtle.end_fill()

# This is the bullseye
turtle.color("yellow")
turtle.penup()
turtle.goto(center_Xvar,center_Yvar-bullseye_radius)
turtle.pendown()
turtle.begin_fill()
turtle.circle(bullseye_radius)
turtle.end_fill()
turtle.hideturtle()

turtle.done()