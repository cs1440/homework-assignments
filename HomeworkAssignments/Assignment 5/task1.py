print("Investing is a smart idea! I am here to help you calculate the profit of your investment")
invest_amount = eval(input("How much money (in dollars) did or will you invest? "))
monthly_payment = eval(input("For the investment, how much is your monthly payment? "))
annual_interest = eval(input("What is the percentage of annual interest? "))/100
years = eval(input("How many years do you intend to have this investment? "))
future_value = invest_amount * (1 + annual_interest / 12)**(years * 12) + monthly_payment * (((1 + annual_interest / 12)**(years * 12) -1) / (annual_interest / 12)) * (1 + annual_interest / 12)

print()
print("Here are the")
print("Investment amount: $" + str(invest_amount))
print("Monthly payment: $" + str(monthly_payment))
print("Annual interest: " , annual_interest * 100 , "%" )
print("Years: " + str(years))
print("Future value of your investment: $" + str(round(future_value, 2)))