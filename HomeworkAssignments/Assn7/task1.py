import random

print("Welcome to the Sensing (Guessing) Game\nThe computer has selected a number between 1 and 10. Try to sense what it is")

randomNumber = random.randint(1,10)
userGuess = int(input("What do you sense the number is from 1-10: "))
guessDifference = abs(userGuess - randomNumber)

if guessDifference == 0:
    output = "You sensed " + str(userGuess) + " and the actual number was " + str(randomNumber) + "." + "\nHonored to play with you, Master Jedi."
elif guessDifference == 1:
    output = "You sensed " + str(userGuess) + " and the actual number was " + str(randomNumber) + "." + "\nYou are a worthy opponent, Jedi Knight."
elif guessDifference == 2:
    output = "You sensed " + str(userGuess) + " and the actual number was " + str(randomNumber) + "." + "\nYou have much to learn, Padawan."
elif guessDifference == 3:
    output = "You sensed " + str(userGuess) + " and the actual number was " + str(randomNumber) + "." + "\nYoungling, your time will come."
else:
    output = "You sensed " + str(userGuess) + " and the actual number was " + str(randomNumber) + "." + "\nKeep working hard in the Service Corps."

print(output + "\n")