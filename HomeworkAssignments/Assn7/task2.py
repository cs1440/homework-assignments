from math import sqrt
from math import pow

print("Welcome to the Social Situation Analyzer system")

# Input for Person one name, location, and radius
print("Person one")
user1Name = input("\tWhat is your name: ")
user1X , user1Y = eval(input("\tEnter your location in terms of x and y: "))
user1Radius = eval(input("\tEnter your personal space radius: "))

# Input for Person two name, location, and radius
print("\nPerson two")
user2Name = input("\tWhat is your name: ")
user2X , user2Y = eval(input("\tEnter your location in terms of x and y: "))
user2Radius = eval(input("\tEnter your personal space radius: "))

distanceFromCenters = sqrt(pow(user2Y - user1Y, 2) + pow(user2X - user1X, 2))

# Personal space test to see who is in whose personal space
if (distanceFromCenters >= user1Radius) and (distanceFromCenters >= user2Radius):
    personTestOutput = ("Neither " + user1Name + " nor " + user2Name + " are in each other's personal space.")
elif (distanceFromCenters <= user1Radius) and (distanceFromCenters >= user2Radius):
    personTestOutput = (user2Name + " is in " + user1Name + "'s personal space.")
elif (distanceFromCenters >= user1Radius) and (distanceFromCenters <= user2Radius):
    personTestOutput = (user1Name + " is in " + user2Name + "'s personal space.")
else:
    personTestOutput = (user1Name + " and " + user2Name + " are in each other's personal space.")

# Test to see if their personal spaces overlap
if (user1Radius + user2Radius < distanceFromCenters):
    spaceTestOutput = (user1Name + " and " + user2Name + "'s personal spaces do not overlap.")
elif (user1Radius + user2Radius > distanceFromCenters):
    spaceTestOutput = (user1Name + " and " + user2Name + "'s personal spaces overlap.")
elif (distanceFromCenters + user1Radius < user2Radius):
    spaceTestOutput = (user1name + "'s personal space is entirely inside " + user2Name + "'s personal space.")
elif (distanceFromCenters + user2Radius < user1Radius):
    spaceTestOutput = (user2name + "'s personal space is entirely inside" + user1Name + "'s personal space.")

# Putting the final print statement together
finalOutput = "\n" + "Social Situation Analysis Results" +"\n\t" + "Person Test: " + personTestOutput + "\n\t" + "Space Test: " + spaceTestOutput

print(finalOutput)