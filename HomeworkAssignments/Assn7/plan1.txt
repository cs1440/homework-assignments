Requirement specifications:
I want to write a program that will the tell user that the computer has selected a random number, and that the user must
then "sense" what that number is. If the user guesses the number exactly it'll say one thing, if they are one off it'll
say another, if they are 2 off it'll say another, 3 off another, and 3 or more it'll have a final phrase

System analysis:
No math functions or calculations will need to be used other than finding out how far away the person is from the random number.
A number from the user will be needed as will a random number from the console

System design:
First import random. Then use the random integer function assign it to be a variable like randomNumber. Then put in code
that evaluates a number that is input from the user as their guess. Set a variable (guessDifference) that is equal to the absolute value of
the difference of the variables. Then start making if and elif statements about the difference. Start off with the smallest
difference of 0, then 1, 2, 3, and 3+. If the difference equals one then have the computer print "Exact match: Honored to play with you, Master."
If the difference is 1 then print "Off by 1: You are a worthy opponent, Knight."
If the difference is 2 then print "Off by 2: You have much to learn, Padawan."
If the difference is 3 then print "Off by 3: Youngling, your time will come."
If the difference is 3+ then print "Off by 3+:Keep working hard in the Service Corps."

Testing:

