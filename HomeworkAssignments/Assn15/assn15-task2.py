from random import randint

# Remember how to use this kind of variable?
count = 0

def main():
    print("Welcome to Recursion Fun")

    # aggienacci calculation
    value = eval(input("Enter a number to find it's aggienacci value: "))
    print("The aggienacci value of " + str(value) + " is " + str(round(aggienacci(value), 4)))

    print()

    # Recursive search and sort
    key = eval(input("Enter a number to search for: "))
    numList = []
    for i in range(200000):
        if randint(0, 2) == 0:
            numList.append(i)

    numPos = binarySearch(numList, key)

    if numPos == -1:
        print("Your number, " + str(key) + ", is not in the list")
    else:
        print("Your number, " + str(key) + ", is in the list at position " + str(numPos))

    print("Total recursive calls: " + str(count))

def aggienacci(value):
    global count
    count += 1
    if value < 0:
        return
    if value == 0:
        return 0
    elif value == 1:
        return 1
    elif value == 2:
        return 2
    return (aggienacci(value-3) + aggienacci(value-2)) / aggienacci(value-1)

def binarySearch(numList, key, marker = 0):
    global count
    count += 1
    begin = -1
    end = len(numList)-1
    mid = (end - begin) // 2
    if len(numList) == 0:
        return -1
    if key == numList[mid]:
        return mid + marker
    elif key > numList[mid]:
        return binarySearch(numList[mid+1:], key, marker = marker + len(numList[:mid+1]))
    elif key < numList[mid]:
        return binarySearch(numList[:mid], key, marker)

main()

