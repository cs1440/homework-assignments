Requirement specification:
Write 2 functions that calculate and "aggienacci" number and another to search through a list to see if a given number is
in it, as well as the index of the number, but only using recursion.

system analysis:
Nothing will need to be imported or looked up the internet. The only thing that is given outside of the code is the aggienacci
formula

System design:
For def aggienacci(num), the base cases will be
if num == 0, if num == 1, and if num == 2. If those are true, they will return 0, 1, and 2 respectively.
The formula will return (aggienacci(num-3) + aggienacci(num-2)) / aggienacci(num-1)
Oh also import global count and add one for every recursion at the beginning of the statement

For def binarySearch(numList, key, tracker = 0(this part is to help calculate index))
import global count into the function and add one for every recursion. then calculate the begin, end, and middle of the list.
Will have 4 if-elif statements. If the items in the list is 0, it will return a -1, if the middle is equal to the key, return
the number plus the tracker. If the key is greater than the number in the middle of the list, call binarySearch and cut off the lesser side of the
list, and add the length of the cut off portion to tracker. If the key is less than the middle of the list, cut off all numbers on the right side of
the list.