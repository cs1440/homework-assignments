from card import Card
from deck import Deck
import time

def main():
    playerMoney = []
    players = eval(input("Enter the number of players (maximum of 5): "))
    while players > 5:
        print("Sorry there can only be 5 players")
        players = eval(input("Enter the number of players (maximum of 5): "))
    for i in range(players):
        playerMoney.append(100)

    people = [x + 1 for x in range(players)]
    keepTrack = [x for x in people]
    playAgain = True

    while playAgain:
        deck = Deck()
        deck.shuffle()
        bet = []
        hands = []
        values = []
        for i in range(len(people)):
            if playerMoney[i] < 5:
                bet.append(playerMoney[i])
                print("Since you have less than $5, you must bet all you have left")
            else:
                playerBet = eval(input("Player " + str(people[i]) + ", place your bet for this round, with a minimum of $5: "))
                while playerBet < 5:
                    playerBet = eval(input("Try again. Your bet must be at least $5: "))
                while playerBet > playerMoney[i]:
                    playerBet = eval(input("You cannot bet more money than you have in your account, try again: "))
                bet.append(playerBet)
        print()
        dealCards(len(people), hands, deck)

# Player gets shown hand and decides to hit or hold

        for i in range(len(people)):
            hit = True
            while hit:
                print()
                print("Here is your hand Player", str(people[i]), hands[i])
                choice = input("Hit or hold? ").lower()
                if choice == "hit":
                    newCard = deck.draw()
                    hands[i].append(newCard)
                    print("\nYour new card is " + str(hands[i][len(hands[i])-1]))
                    hit = bust(hands, values, i)
                else:
                    values.append(handValue(hands, i))
                    hit = False

# This is where the dealer gets dealt a card
        print("Here are the dealer's current cards")
        for i in hands[len(hands)-1]:
            print(i)

        dealer = True
        if handValue(hands, len(hands)-1) > 17:
            print("The dealer holds")
        while handValue(hands, len(hands)-1) < 17:
            print()
            time.sleep(1)
            newCard = deck.draw()
            hands[len(people)].append(newCard)
            print("The dealer takes a card")
            if handValue(hands, len(people)) > 21:
                print("The dealer busts")
                dealer = False
            elif (handValue(hands, len(people)) >= 17) and (handValue(hands, len(people)) <=21):
                print("The dealer holds")
            else:
                continue

        values.append(handValue(hands, len(people)))

        for i in range(len(people)):
            print()
            if (values[i] < values[len(people)] and dealer) or (values[i] > 21 and dealer):
                print("Player " + str(people[i]) + ": You have lost to the dealer. Your score was " + str(values[i]) + " and the dealer's was " + str(values[len(people)]))
                playerMoney[i] -= bet[i]
            elif ((values[i] > values[len(people)]) and (values[i] <= 21)) or (values[i] <= 21 and not dealer):
                print("Player " + str(people[i]) + ": You have beat to the dealer! Your score was " + str(values[i]) + " and the dealer's was " + str(values[len(people)]))
                playerMoney[i] += bet[i]
            elif (values[i] == values[len(people)] and dealer) or (values[i] > 21 and not dealer):
                print("Player " + str(people[i]) + ": You and the dealer have tied!")
        idx = 0
        print()
        for i in playerMoney:
            print("Player " + str(keepTrack[idx]) + "'s current balance: $" + str(i))
            idx += 1
        print()
        for i in range(len(playerMoney)):
            if playerMoney[i] == 0 and len(playerMoney) == len(people):
                people.pop(i)
                keepTrack[i], keepTrack[len(keepTrack)-1] = keepTrack[len(keepTrack)-1], keepTrack[i]
                playerMoney[i], playerMoney[len(keepTrack)-1] = playerMoney[len(keepTrack)-1], playerMoney[i]

        play = input("Would you like to continue playing? ").lower()
        if play == "yes":
            playAgain = True
        else:
            playAgain = False


    for i in range(len(playerMoney)):
        for j in range(len(playerMoney)-1):
            if playerMoney[j] < playerMoney[j+1]:
                playerMoney[j], playerMoney[j+1] = playerMoney[j+1], playerMoney[j]
                keepTrack[j], keepTrack[j+1] = keepTrack[j+1], keepTrack[j]
    print("\nThanks for playing! Here are your results:")
    for i in range(len(playerMoney)):
        print("Player " + str(keepTrack[i]) + ": $" + str(playerMoney[i]))


def dealCards(players, hands, deck):
    for i in range(players+1):
        hands.append([])
    for i in range(2):
        for j in range(players+1):
            hands[j].append(deck.draw())
    print("The dealer's top card is" , hands[players][1])

def bust(hands, values, player):
    playerValue = handValue(hands, player)
    for i in hands[player]:
        if i.getCardValue() == 1 and playerValue > 21:
            playerValue -= 10
    if playerValue > 21:
        print("Player " + str(player + 1) + ", your point value is over 21. You have busted")
        values.append(handValue(hands, player))
        return False
    else:
        return True

def handValue(hand, player):
    val = 0
    for i in hand[player]:
        if i.getCardValue() > 10:
            val += 10
        elif (i.getCardValue() == 1) and ((val + 11) < 21):
            val += 11
        else:
            val += i.getCardValue()
    return val









main()
