from cards import Card
from cards import Deck
import gronkyutil


def main():
    print("Welcome to the Gronky game! Why is it named this? I don't know!")
    input("Press enter to continue")
    deck = Deck()
    playerCards = dealCards(deck)
    print("\nHere is your deck of cards: ")
    print()
    for i in playerCards:
        print(i)

    done = False

    while not done:
        print()
        print("Menu")
        print("\t1) Sort by value")
        print("\t2) Sort by id")
        print("\t3) Find card")
        print("\t4) New hand")
        print("\t5) Quit")
        choice = eval(input("Make a selection: "))

        if choice == 1:
            sortByValue(playerCards)
        elif choice == 2:
            sortById(playerCards)
        elif choice == 3:
            findCard(playerCards)
        elif choice == 4:
            playerCards = newHand(deck)
            print("\nHere is your new hand:")
            for i in playerCards:
                print(i)
        else:
            done = True
    print()
    print("Thanks for playing the game! Hope you had fun!")


def dealCards(deck):
    deck.shuffleDeck()
    cards = []
    for i in range(30):
        oneCard = deck.drawCard()
        cards.append(oneCard)
    return cards


def sortByValue(hand):
    for i in range(len(hand)):
        key = hand[i]
        num = int(hand[i].split()[0])
        j = i-1
        while j >= 0 and num < int(hand[j].split()[0]):
            hand[j+1] = hand[j]
            j -= 1
        hand[j+1] = key
    for k in hand:
        print(k)

def sortById(hand):
    hand = bubbleSort(hand)
    for i in hand:
        print(i)

def findCard(deck):
    val = eval(input("Value of card: "))
    hand = input("Rock, Paper, or Scissors: ").title()
    coin = input("Heads or tails: ").title()
    cardChoice = str(val) + " of " + hand + " " + coin
    sortedDeck = bubbleSort(deck)
    print(binarySearch(sortedDeck, cardChoice, val))


def newHand(deck):
    return dealCards(deck)

def binarySearch(deck, key, val):
    begin = 0
    end = len(deck) - 1
    while end >= begin:
        middle = (begin + end) // 2
        if deck[middle] == key:
            return key + " is in your hand"
        elif val > int(deck[middle].split()[0]):
            begin = middle + 1
        else:
            end = middle - 1
    return key + " is not in your hand"


def bubbleSort(hand):
    for i in range(len(hand)):
        for j in range(len(hand)-1):
            card1Words = hand[j].split()
            card2Words = hand[j+1].split()
            if gronkyutil.convertCardToID(int(card1Words[0]), card1Words[2], card1Words[3]) > gronkyutil.convertCardToID(int(card2Words[0]), card2Words[2], card2Words[3]):
                hand[j], hand[j+1] = hand[j+1], hand[j]
    return hand


main()
