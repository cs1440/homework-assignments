# Call your file assn14-task2.py. Write a program that will prompt the user to enter a number. Keep collecting numbers until the user hits enter without entering a number. Once the user hits enter without entering a number, calculate and display the following:
#
# Number of values entered
# Maximum value. Write your own code. Do not use a built-in function.
# Minimum value. Use a built-in function
# Sum of all values. Use a loop to calculate.
# Average value. Use your calculated sum to calculate.

def main():
    number = eval(input("Enter a number: "))
    list = []
    list.append(number)
    while True:
        number = input("Enter a number: ")
        if number == "":
            break
        else:
            number = eval(number)
            list.append(number)
    sortedList = bubbleSort(list)
    print()
    print("You entered this many values: " + str(lengthOfList(sortedList)))
    print("The maximum value of your list is " + str(maxValue(sortedList)))
    print("The minimum value of your list is " + str(minValue(sortedList)))
    print("The sum of all your values is " + str(sumValues(sortedList)))
    print("The average value of your list is " + str(avgValue(sortedList)))

def bubbleSort(list):
    for x in range(len(list)):
        for i in range(len(list)-1):
            if list[i] > list[i+1]:
                list[i], list[i+1] = list[i+1], list[i]
    return list

def maxValue(list):
    return list[len(list)-1]

def minValue(list):
    return min(list)

def sumValues(list):
    sum = 0
    for i in range(len(list)):
        sum += list[i]
    return sum

def avgValue(list):
    return sumValues(list) / len(list)

def lengthOfList(list):
    return len(list)

main()

