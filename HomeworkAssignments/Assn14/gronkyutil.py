paw = ["Rock", "Paper", "Scissors"]
coin = ["Heads", "Tails"]
maxCardValue = 20

# Make sure you understand this to do the opposite conversion!!!


def convertCardToID(cardValue, cardPaw, cardCoin):
    return 2 * ((cardValue - 1) + (maxCardValue * paw.index(cardPaw))) + coin.index(cardCoin)


def convertIDToCard(id):
    for p in paw:
        for val in range(1, 21):
            for c in coin:
                if id == convertCardToID(val, p, c):
                    return val, p, c
