from random import shuffle
import gronkyutil

class Card:
    def __init__(self, id):
        self.__id = id

    def getCardVal(self):
        for p in gronkyutil.paw:
            for val in range(1, 21):
                for c in gronkyutil.coin:
                    if id == gronkyutil.convertCardToID(val, p, c):
                        return val

    def getCoin(self):
        for p in gronkyutil.paw:
            for val in range(1, 21):
                for c in gronkyutil.coin:
                    if id == gronkyutil.convertCardToID(val, p, c):
                        return c

    def getPaw(self):
        for p in gronkyutil.paw:
            for val in range(1, 21):
                for c in gronkyutil.coin:
                    if id == gronkyutil.convertCardToID(val, p, c):
                        return p


class Deck:

    def __init__(self):
        self.__val = [x for x in range(1, 21)]
        self.__fullDeck = []
        for i in range(20):
            for j in range(2):
                for k in range(3):
                    card = ""
                    card += str(self.__val[i]) + " of "
                    card += gronkyutil.paw[k] + " "
                    card += gronkyutil.coin[j]
                    self.__fullDeck.append(card)

    def shuffleDeck(self):
        shuffle(self.__fullDeck)

    def drawCard(self):
        return self.__fullDeck.pop()




