import random


class Cards:
    def __init__(self):
        self.suits = ["Clubs", "Hearts", "Spades", "Diamonds"]
        self.numbers = ["Ace", "1", "2", "3", "4", "5", '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King']
        self.deck = []

    def createDeck(self):
        for i in self.suits:
            for j in self.numbers:
                self.deck.append(j + " of " + i)

    def shuffleDeck(self):
        self.createDeck()
        newDeck = []
        for i in range(52, 0, -1):
            newDeck.append(self.deck.pop(random.randint(0, i)))
        self.deck = newDeck

