from hi import Cards


def main():
    deck = Cards()
    deck.shuffleDeck()

    print("Welcome to this card game!")
    players = eval(input("How many people are playing the game? "))
    notDone = True
    playerMoney = [100 for x in range(players)]
    while notDone:
        for x in range(len(playerMoney)):
            print("Player" + str(x + 1) + " current funds: $" + str(playerMoney[x]))
        playerBets = [x for x in range(players)]
        for i in range(players):
            done = False
            while not done:
                bet = int(input("Player " + str(i + 1) + ", Place a bet: "))
                if bet > playerMoney[i]:
                    print("You can't bet more money than you have")
                    done = False
                else:
                    playerBets[i] = bet
                    done = True
        playerCards = [[] for x in range(players)]
        for i in range(2):
            for j in range(players):
                playerCards[j].append(deck.deck.pop())
        dealerHand = [deck.deck.pop(), deck.deck.pop()]
        displayPlayers(playerCards)
        


def displayPlayers(cards):
    for i in range(len(cards)):
        print("Player " + str(i + 1) + " stats: ")
        for j in range(len(cards[i])):
            print(cards[i][j])



main()

