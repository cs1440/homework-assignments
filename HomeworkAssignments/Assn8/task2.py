from random import randint

while True:
    totalTests = 0
    loneElephantTotal = 0
    bothElephantTotal = 0
    while totalTests < 100001:
        elephantOne = randint(1,6)
        elephantTwo = randint(1,6)
        zookeeperPick = randint(1,6)
        if (zookeeperPick == elephantOne) or (zookeeperPick == elephantTwo):
            loneElephantTotal += 1
            if elephantOne == elephantTwo:
                bothElephantTotal += 1
        totalTests += 1
    loneDecimal = loneElephantTotal/100000
    lonePercent = format(loneDecimal, ".2%")
    bothDecimal = bothElephantTotal/loneElephantTotal
    bothPercent = format(bothDecimal, ".2%")
    marginOfError1 = abs(float(loneDecimal*100) - float(100/3))
    marginOfError2 = abs(float(bothDecimal*100) - float(100/6))

    print("Percent of times at least one elephant was in a pen:", lonePercent, "\nPercent of times both elephants were in a pen after at least one was discovered in a pen:", bothPercent)


    if (marginOfError1 < 2) and (marginOfError2 < 2):
        print("The zookeepers margin of error is less than 2%, therefore the zookeeper is correct!")

    else:
        print("Since the zookeepers margin of error is not within 2%, the custodian is right")

    if input("Would you like to repeat the simulation? (yes/no) ").lower() == "yes":
        print()
        continue
    else:
        break