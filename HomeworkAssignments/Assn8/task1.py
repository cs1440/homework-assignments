from random import randint
from random import seed
from math import sqrt
import time

start = time.time()

totalFluky = 8
startingFluky = 0
numberOfLoops = 0
for i in range(8,10001):
    randomNumberTotal = 0
    for j in range (2, int(sqrt(i))+1):
        numberOfLoops += 1
        if i % j == 0:
            seed(1)
            randomNumberTotal += randint(1,i)
            seed(j)
            randomNumberTotal += randint(1,i)
            if i/j == j:
                continue
            seed(i/j)
            randomNumberTotal += randint(1,i)
    if randomNumberTotal == i:
        startingFluky += 1
        if startingFluky == totalFluky:
            break
        else:
            print("Fluky Number: ", i)

finish = time.time()
print("Total Time:", round(finish-start,2), "seconds")
print("Number of loops:", numberOfLoops)

