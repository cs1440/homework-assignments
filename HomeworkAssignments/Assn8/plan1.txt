Requirement specification:
This program should find all fluky numbers between 1 and 10,000.

System analysis:
Need to import random and need to know how to find the factors of a number

System Design:
First import randint and seed from the random module. create a while loop that runs as long as you have less than 8 Fluky numbers
Write code that will find out a number's factors. To do so, write a for loop that goes between 1 and the square root of the initial number plus 1
In the loop, mod the number by the number that the loop is testing for. Inside do an if statement that says if the mod is equal to 0
then that number is a factor and assign it a variable then test the seed and randint, else it will go to the next number to test.
Outside that if statement calculate the randint based on the seed factor and add it to the total number, then have test to see
if the total equals the original number. If so, it is a fluky number. Repeat the process. Import time at the beginning and end and
subtract the two to find how long it took.