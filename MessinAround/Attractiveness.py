# This program is designed to calculate the overall attractiveness of a person by using 3 different factors

input("Welcome to the attractiveness calculator!!! I will calculate someone's attractiveness according to the different variables.\
      \nClick this line and press enter to continue ")

physicalImportance , personalityImportance , ambitionImportance = eval(input("On individual scales of 1-10, how important are each of the following to you: physical attractiveness, personality, life ambition.(ex.5, 8, 5) "))
name = input("What is the name of the person who we will be assessing? ")
physicalAttractiveness = eval(input("What is " + name + "'s physical attractiveness on a scale of 1-10? "))
personality = eval(input("Rate " + name + "'s personality on a scale of 1-10: "))
lifeAmbition = eval(input("Rate " + name + "'s life ambitions on a scale of 1-10: "))

physicalDecimal = (physicalImportance)/(physicalImportance + personalityImportance + ambitionImportance)
personalityDecimal = (personalityImportance)/(physicalImportance + personalityImportance + ambitionImportance)
ambitionDecimal = (ambitionImportance)/(physicalImportance + personalityImportance + ambitionImportance)

overallAttractiveness = (physicalDecimal*physicalAttractiveness) + (personalityDecimal * personality) + (ambitionDecimal * lifeAmbition)

print(name + "'s overall attractiveness on a scale of 1-10 is " + str(round(overallAttractiveness,1)))