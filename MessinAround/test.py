'''
Define a function called numPyramid but don't give it any parameters. Inside have an variable "rows" equal to an evaluated input asking
how many rows the user wants to have. Then make a variable "formatRows" equal to the string of the number and have it be center
justified
Then have a for loop that goes i in range between 1 and "rows". Create a variable timesPrinted = 0 and then create a
while loop that will run as long as timesPrinted < i. Inside that loop print i formatted by "formatRows" and have end=" "
Then have timesPrinted += 1

'''

def numPyramid(rows = int(input("Enter the desired number of rows: "))):
    if rows < 10:
        formatRows = "^" + str(rows*2)
    else:
        formatRows = "^" + str(rows*3)
    for i in range(1,rows+1):
        print(format((str(i)+" ")*i, formatRows))
numPyramid()
