bill = eval(input("How much money is owed? "))
paid = eval(input("How much money was paid? "))
amount = paid - bill

totalCents = int(amount * 100)

numberOfDollars = totalCents // 100
remainingAmount = totalCents % 100

numberOfQuarters = remainingAmount // 100
remainingAmount = remainingAmount % 25

numberOfDimes = remainingAmount // 10
remainingAmount = remainingAmount % 10

numberOfNickles = remainingAmount // 5
remainingAmount = remainingAmount % 5

numberOfPennies = remainingAmount

print("Your amount" , amount , "consists of\n" ,
      "\t" , numberOfDollars , "dollars\n"
      "\t" , numberOfQuarters , "quarters\n"
      "\t" , numberOfDimes , "dimes\n"
      "\t" , numberOfNickles , "nickles\n"
      "\t" , numberOfPennies , "pennies\n")

