name = "Andy Brim"
print(name.upper())
print(name.lower())

# Strips off extra unused space
print(name.strip())
print(name.rstrip())
print(name.lstrip())


print(format(57.4564564, ".f"))